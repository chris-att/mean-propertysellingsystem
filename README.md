## **An example of a MEAN application.**

## Demo
-	Access on https://real-estate-system.firebaseapp.com
-	Employee: employee1@aproperties.com | Password1234
-	User: user1@gmail.com | Password1234

---

Included in this repositry;
-	Backend Server in JAVASCRIPT.
-	Frontend(SPA) in Angular 5.(Uses HTTPClient)

*Node v9.2.0, NPM v5.5.1*

---

## Backend

-	MongoDB w/ Mongoose modelling.
-	Authentication using JWT & Passport.
-	RESTful API via Routes.
-	Routes are protected via JWT claims ∴ only authorised users can access.

---

## Frontend

-	CSSOnly - https://www.npmjs.com/package/ng-surface
-	Protected routes via guards.
-	HTTP Interceptor service to add JWT token automatically.
-	Reactive Forms.

---