import { Property } from "./property";

export interface Page {
    docs:Property[];
    total:number;
    limit:number;
    page:number;
    pages:number;
}