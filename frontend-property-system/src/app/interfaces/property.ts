export interface Property {
    _id:string;
    pLocation:string;
    pPrice:number;
    pType:string;
    isResidential:boolean;
    pDescription:string;
    pImage:string;
}
