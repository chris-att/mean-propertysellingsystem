export interface User {
    firstName:string;
    lastName:string;
    email:string;
    phone:string;
    password:string;
    sendNotifications:string;
    sendCatalog:boolean;
    isEmployee:boolean;
}