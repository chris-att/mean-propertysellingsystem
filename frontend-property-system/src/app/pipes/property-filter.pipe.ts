import { Pipe, PipeTransform } from '@angular/core';
import { Property } from '../interfaces/property';

@Pipe({
  name: 'filter'
})
export class PropertyFilterPipe implements PipeTransform {

  transform(properties:Property[], searchText:string):Property[] {
    if(!properties) {
      return [];
    } else {
      if(!searchText){ 
        return properties;
      } else {
        return properties.filter(property => property.pLocation.toLowerCase().includes(searchText.toLowerCase()));
      }
    }
  }

}
