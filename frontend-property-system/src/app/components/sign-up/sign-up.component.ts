import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { UserService } from '../../services/user-service.service';
import { User } from '../../interfaces/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  user: User;
  signupForm: FormGroup;
  constructor(private fb: FormBuilder, private userService: UserService, private router: Router) {
  }

  ngOnInit() {

    this.signupForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      emailGroup: this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        confirmMail: ['', Validators.required]
      }, { validator: this.childrenEqual }),
      password: ['', [Validators.required, Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}')]],
      phone: ['',[Validators.required,Validators.pattern('[0-9]{8}')]],
      sendNotifications: ['email'],
      sendCatalog:[false]
    });
  }

  onSubmit() {
    if (this.signupForm.valid) {
      console.log('valid');
      console.log(this.signupForm.value);
    } else {
      console.log('invalid');
      console.log(this.signupForm);
    }
  }

   childrenEqual: ValidatorFn = (formGroup: FormGroup) => {
    const [firstControlName, ...otherControlNames] = Object.keys(formGroup.controls || {});
    const isValid = otherControlNames.every(controlName => formGroup.get(controlName).value === formGroup.get(firstControlName).value);
    return isValid ? null : { childrenNotEqual: true };
  }

  register():void{
    this.user = {firstName : this.signupForm.get('firstName').value,
                 lastName : this.signupForm.get('lastName').value,
                 email : this.signupForm.get('emailGroup.email').value,
                 phone : this.signupForm.get('phone').value,
                 password : this.signupForm.get('password').value,
                 sendNotifications : "email",
                 sendCatalog : this.signupForm.get('sendCatalog').value,
                 isEmployee : false}
    this.userService.createUser(this.user)
    .subscribe(
      data => {this.router.navigate(['/login']);},
      error => {console.log(error.message)}
    )
  }

}
