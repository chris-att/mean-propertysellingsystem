import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit(){
    this.isLoggedIn();
    this.changeDetector.detectChanges();
  }

  ngAfterViewInit() {
    this.changeDetector.detectChanges();
  }

  isLoggedIn(){
    if(localStorage.getItem("currentUser") === null)
  {
    return false;
  }
  else return true;
  }

}
