import { Component, OnInit } from '@angular/core';
import { PropertyService } from '../../services/property-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,FormBuilder,FormControl,Validators } from '@angular/forms';
import { Property } from '../../interfaces/property';

@Component({
  selector: 'app-update-property',
  templateUrl: './update-property.component.html',
  styleUrls: ['./update-property.component.css']
})
export class UpdatePropertyComponent implements OnInit {

  updatePropertyForm: FormGroup;
  property: Property;
  pId: string;

  constructor(private aRoute: ActivatedRoute ,private fb: FormBuilder, private ps: PropertyService, private router: Router) { }

  ngOnInit() {
    this.updatePropertyForm = this.fb.group({
      pLocation: ['', [Validators.required, Validators.minLength(3)]],
      pPrice: ['', [Validators.required, Validators.pattern('[0-9]{3,}')]],
      pType: ['', [Validators.required, Validators.minLength(3)]],
      pDescription: ['', [Validators.required, Validators.minLength(3)]],
      pSrc: ['', [Validators.required, Validators.pattern('^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|(www\\.)?){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?')]],
      isResidential: [false]
    });
    this.property={
      _id:"",
      pLocation:"",
      pPrice:0,
      pType:"",
      isResidential:false,
      pDescription:"",
      pImage:""
        };
    this.aRoute.params.subscribe(params => {this.pId = params['id']});
    this.getPropertyDetails();
  }

  getPropertyDetails():void{
    this.ps.getPropertyDetails(this.pId).subscribe(
      property => {
        this.property = property;
      },
      error => {
        console.log(error.message);
      }
    )
  }

  updateProperty():void{
    this.property = {
      _id: this.pId,
      pLocation: this.updatePropertyForm.get('pLocation').value,
      pPrice: this.updatePropertyForm.get('pPrice').value,
      pType: this.updatePropertyForm.get('pType').value,
      isResidential: this.updatePropertyForm.get('isResidential').value,
      pDescription: this.updatePropertyForm.get('pDescription').value,
      pImage: this.updatePropertyForm.get('pSrc').value
    }
    this.ps.editProperty(this.property)
    .subscribe(
      data => {this.router.navigate(['/properties/'+this.property._id]);},
      error => {console.log(error.message)}
    )
  }

}