import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PropertyService } from '../../services/property-service.service';
import { Property } from '../../interfaces/property';

@Component({
  selector: 'app-add-property',
  templateUrl: './add-property.component.html',
  styleUrls: ['./add-property.component.css']
})
export class AddPropertyComponent implements OnInit {

  updatePropertyForm: FormGroup;
  property: Property;
  pId: string;

  constructor(private aRoute: ActivatedRoute ,private fb: FormBuilder, private ps: PropertyService, private router: Router) { }

  ngOnInit() {
    this.updatePropertyForm = this.fb.group({
      pLocation: ['', [Validators.required, Validators.minLength(3)]],
      pPrice: ['', [Validators.required, Validators.pattern('[0-9]{3,}')]],
      pType: ['', [Validators.required, Validators.minLength(3)]],
      pDescription: ['', [Validators.required, Validators.minLength(3)]],
      pSrc: ['', [Validators.required, Validators.pattern('^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|(www\\.)?){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?')]],
      isResidential: [false]
    });
  }

  addProperty():void{
    this.property = {
      _id: "",
      pLocation: this.updatePropertyForm.get('pLocation').value,
      pPrice: this.updatePropertyForm.get('pPrice').value,
      pType: this.updatePropertyForm.get('pType').value,
      isResidential: this.updatePropertyForm.get('isResidential').value,
      pDescription: this.updatePropertyForm.get('pDescription').value,
      pImage: this.updatePropertyForm.get('pSrc').value
    }
    this.ps.addProperty(this.property)
    .subscribe(
      data => {this.router.navigate(['/properties/']);},
      error => {
        console.log(error.message)
      }
    )
  }

}
