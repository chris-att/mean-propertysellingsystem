import { Component, OnInit } from '@angular/core';
import { Property } from '../../interfaces/property';
import { PropertyService } from '../../services/property-service.service';
import { Page } from '../../interfaces/i-page';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.component.html',
  styleUrls: ['./property-list.component.css']
})
export class PropertyListComponent implements OnInit {

  properties: Property[];
  page: Page;
  loggedUser: User;
  searchText: string;

  constructor(private ps: PropertyService) {
    this.properties = [];
    this.page = null;
   }

  ngOnInit() {
    this.populatePage(1);
  }

  populatePage(pagenumber: number):void{
    this.ps.getPage(pagenumber).subscribe(
      page => {
        this.page = page;
        this.properties = page.docs;
      },
      error => {
        console.log(error.message)
      }
    );
  }

  deleteProperty(pId: string, pagenumber: number, totalpages: number):void{
    this.ps.deleteProperty(pId).subscribe(
      error => {
        console.log(error.message)
      }
    );
    if(pagenumber<=totalpages){
      this.populatePage(pagenumber);
    }
    else
    {
      this.previousPage(pagenumber);
    }
  }

  nextPage(pagenumber: number):void
  {
    if(pagenumber == this.page.pages)
    {
      this.populatePage(this.page.pages);
    }
    else{
      this.populatePage(++pagenumber)
    }
  }

  previousPage(pagenumber:number):void
  {
    if(pagenumber == 1)
    {
      this.populatePage(1);
    }
    else{
      this.populatePage(--pagenumber);
    }
  }

  isEmployee(){
    if(localStorage.getItem("currentUser") === null)
    {
      return false;
    }
    
    let currentUser = localStorage.getItem("currentUser");
     this.loggedUser = JSON.parse(currentUser);
    if(this.loggedUser.isEmployee){
      return true;
    }
    else{
      return false;
    }
  }

}
