import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../interfaces/user';
import {Location} from '@angular/common';
import { Property } from '../../interfaces/property';
import { PropertyService } from '../../services/property-service.service';

@Component({
  selector: 'app-property-detail',
  templateUrl: './property-detail.component.html',
  styleUrls: ['./property-detail.component.css']
})
export class PropertyDetailComponent implements OnInit {

  propertyId: string;
  loggedUser: User;
  property: Property;

  constructor(private ps: PropertyService, private aRoute:ActivatedRoute, private _location: Location) { 
    
  }

  ngOnInit() {
    //this.isEmployee();
    this.aRoute.params.subscribe(params => { this.propertyId = params['id'] });
    this.property={
    _id:"",
    pLocation:"",
    pPrice:0,
    pType:"",
    isResidential:false,
    pDescription:"",
    pImage:""
      };
    this.getPropertyDetails();
  }

  isEmployee(){
    let currentUser = localStorage.getItem("currentUser");
     this.loggedUser = JSON.parse(currentUser);
    if(this.loggedUser.isEmployee){
      return true;
    }
    else{
      return false;
    }
  }

  getPropertyDetails():void{
    this.ps.getPropertyDetails(this.propertyId).subscribe(
      property => {
        this.property = property;
        console.log(property);
      },
      error => {
        console.log(error.message);
      }
    )
  }
}
