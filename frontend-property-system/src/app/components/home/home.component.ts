import { Component, OnInit } from '@angular/core';
import { PropertyService } from '../../services/property-service.service';
import { Property } from '../../interfaces/property';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  properties: Property[];

  constructor(private ps:PropertyService){
    this.properties = [];
  }

  ngOnInit() {
    this.populateProperties();
  }

  populateProperties():void{
    this.ps.getProperties().subscribe(
      properties => {
        this.properties = properties
      },
      error => {
        console.log(error.message)
      }
    );
  }

}
