import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Property } from '../interfaces/property';
import { RequestOptions, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import { Page } from '../interfaces/i-page';
import { environment } from '../../environments/environment';

@Injectable()
export class PropertyService {

  private urlgetLastProperties: string;
  private urlgetProperties: string;
  private urlgetPropertiesByPage: string;
  private urldeleteProperty: string;
  private urladdProperty: string;
  private urleditProperty: string;
  private urlgetPropertyDetails: string;

  constructor(private http:HttpClient) {
    this.urlgetLastProperties = environment.apiEndpoint+'/properties/getLastProperties';
    this.urlgetProperties = environment.apiEndpoint+'/properties/';
    this.urlgetPropertiesByPage = environment.apiEndpoint+'/properties/page/';
    this.urldeleteProperty = environment.apiEndpoint+'/properties/deleteProperty/';
    this.urladdProperty = environment.apiEndpoint+'/properties/addProperty';
    this.urleditProperty = environment.apiEndpoint+'/properties/updateProperty';
   }

   public getProperties():Observable<Property[]>{
     return this.http.get<Property[]>(this.urlgetLastProperties);
   }

   public getAllProperties(): Observable<Property[]>{
     return this.http.get<Property[]>(this.urlgetProperties);
   }

   public getPage(pagenumber: number):Observable<Page>{
     return this.http.get<Page>(this.urlgetPropertiesByPage +pagenumber)
     .map(res => {return res["data"]})
   }

   public deleteProperty(pId: string):Observable<any>{
    return this.http.delete(this.urldeleteProperty+pId);
   }

   public getPropertyDetails(_id: string):Observable<Property>{
     console.log(_id);
    return this.http.get<Property>(this.urlgetProperties + _id);
  }

   public addProperty(property:Property):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(this.urladdProperty, JSON.stringify(property), httpOptions);
   }

   public editProperty(property: Property):Observable<any>
    {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      };
      console.log(property);
      return this.http.put(this.urleditProperty, JSON.stringify(property), httpOptions);
  }
}