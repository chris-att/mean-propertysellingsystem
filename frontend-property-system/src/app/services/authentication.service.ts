import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../interfaces/user';
import 'rxjs/add/operator/map'
import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {

  constructor(private http:HttpClient) { }

  login(email: string, password: string) {
    return this.http.post<any>(environment.apiEndpoint+'/users/auth', { email: email, password: password})
      .map(user => {
        //login successful if there is a JWT token in the reponse.
        if(user && user.token) {
          //store user details and JWT token in local storage to keep user loggin in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          console.log(user);
        }
        return user;
      }
      );
  }

  logout(){
    localStorage.removeItem('currentUser');
  }
}
