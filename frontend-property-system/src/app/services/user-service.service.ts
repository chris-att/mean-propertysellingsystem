import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { User } from '../interfaces/user';
import { Observable } from 'rxjs/Observable';
import { RequestOptions, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment.prod';

@Injectable()
export class UserService {

  private urlcreateUser: string;

  constructor(private http: HttpClient) {
    this.urlcreateUser = environment.apiEndpoint+'/users/register';
   }

  public createUser(user: User):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
    console.log(JSON.stringify(user));
    return this.http.post(this.urlcreateUser, JSON.stringify(user), httpOptions);
  }
}
