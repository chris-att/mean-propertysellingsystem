import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { PropertyListComponent } from './components/property-list/property-list.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { PropertyService } from './services/property-service.service';
import { ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from './services/authentication.service';
import { JwtInterceptorService } from './helpers/jwt-interceptor.service';
import { PropertyDetailComponent } from './components/property-detail/property-detail.component';
import { UserService } from './services/user-service.service';
import { UpdatePropertyComponent } from './components/update-property/update-property.component';
import { AddPropertyComponent } from './components/add-property/add-property.component';
import { AuthGuardGuard } from './guards/auth-guard.guard';
import { EmployeeGuard } from './guards/employee.guard';
import { AlertComponent } from './directives/alert/alert.component';
import { AlertService } from './services/alert.service';
import { PropertyFilterPipe } from './pipes/property-filter.pipe';
import { NavigationComponent } from './components/navigation/navigation.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutUsComponent,
    PropertyListComponent,
    SignUpComponent,
    LogInComponent,
    PropertyDetailComponent,
    UpdatePropertyComponent,
    AddPropertyComponent,
    AlertComponent,
    PropertyFilterPipe,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production})
  ],
  providers: [
    AuthenticationService,
    PropertyService,
    UserService,
    AuthGuardGuard,
    EmployeeGuard,
    AlertService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorService,
      multi: true
    },
    FormBuilder],
  bootstrap: [AppComponent]
})
export class AppModule { }
