import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { User } from '../interfaces/user';

@Injectable()
export class EmployeeGuard implements CanActivate {

  loggedUser: User;

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.isEmployee()) {
        // logged in so return true
        return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['home'], { queryParams: { returnUrl: state.url }});
    return false;
}

isEmployee(){
  let currentUser = localStorage.getItem("currentUser");
   this.loggedUser = JSON.parse(currentUser);
  if(this.loggedUser.isEmployee){
    return true;
  }
  else{
    return false;
  }
}

}
