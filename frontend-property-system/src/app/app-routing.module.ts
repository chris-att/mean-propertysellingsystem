import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { PropertyListComponent } from './components/property-list/property-list.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { PropertyDetailComponent } from './components/property-detail/property-detail.component';
import { UpdatePropertyComponent } from './components/update-property/update-property.component';
import { AddPropertyComponent } from './components/add-property/add-property.component';
import { AuthGuardGuard } from './guards/auth-guard.guard';
import { EmployeeGuard } from './guards/employee.guard';

const routes: Routes = [
  //{path: '', component: HomeComponent, canActivate: [AuthGuardGuard] },
  {path:"home",component:HomeComponent},
  {path:"aboutus",component:AboutUsComponent},
  {path:"properties",component:PropertyListComponent, canActivate: [AuthGuardGuard]},
  {path:"properties/:id",component:PropertyDetailComponent, canActivate: [AuthGuardGuard]},
  {path: "editProperty/:id",component:UpdatePropertyComponent, canActivate: [AuthGuardGuard,EmployeeGuard]},
  {path: "addProperty", component:AddPropertyComponent, canActivate: [AuthGuardGuard,EmployeeGuard]},
  {path:"signup",component:SignUpComponent},
  {path:"login",component:LogInComponent},
  {path:"logout",component:LogInComponent},
  {path:"**",redirectTo:"home"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
