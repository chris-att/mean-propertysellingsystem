export const environment = {
  production: true,
  apiEndpoint: 'https://us-central1-real-estate-system.cloudfunctions.net/api'
};
