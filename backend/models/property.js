let mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
let Schema = mongoose.Schema;

//The model for the property in the database.
var propertySchema = new Schema(
    {
        pLocation: {
            type: String,
            required: true,
        },
        pPrice: {
            type: Number,
            required: false,
        },
        pType: {
            type: String,
            required: true,
        },
        isResidential: {
            type: Boolean,
            required: true,
        },
        pDescription: {
            type: String,
            required: true,
        },
        pImage: {
            type: String,
            required: true,
        }
    }
);

propertySchema.plugin(mongoosePaginate);

//Export the model
module.exports = mongoose.model('Property', propertySchema);