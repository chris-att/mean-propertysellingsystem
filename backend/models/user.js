let mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
let Schema = mongoose.Schema;

//The model for the user in the database.
var UserSchema = new Schema(
    {
        firstName: {
            type: String,
            required: true,
        },
        lastName: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            unique: true,
            required: true,
        },
       phone: {
            type: String,
            required: true,
        },
        password: {
            type: String,
            required: true,
        },
        sendNotifications: {
            type: String,
            required: true,
        },
        sendCatalog: {
            type: Boolean,
            required: true,
        },
        isEmployee: {
            type: Boolean,
            required: true,
        }
    }
);

// Hash the user's password before inserting a new user
UserSchema.pre('save', function(next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
      bcrypt.genSalt(10, function(err, salt) {
        if (err) {
          return next(err);
        }
        bcrypt.hash(user.password, salt, function(err, hash) {
          if (err) {
            return next(err);
          }
          user.password = hash;
          next();
        });
      });
    } else {
      return next();
    }
  });

  // Compare password input to password saved in database
UserSchema.methods.comparePassword = function(pw, cb) {
    bcrypt.compare(pw, this.password, function(err, isMatch) {
      if (err) {
        return cb(err);
      }
      cb(null, isMatch);
    });
  };
  
  // Export the model
module.exports = mongoose.model('User', UserSchema);