var express = require('express');
var router = express.Router();
var Property = require('../models/property');

let passport = require('passport');
let jwt = require('jsonwebtoken');

let config = require('../config');

// Register new properties
router.post('/addProperty', passport.authenticate('jwt', {
    session: false,
  }), function(req, res){
    console.log(req.user.email);
    if (!req.user.isEmployee) {
        res.json(
            {
                success: false,
                message: 'This space in unauthorised.'
            }
        );
    } else {
        let newProperty = new Property({
            pLocation: req.body.pLocation,
            pPrice: req.body.pPrice,
            pType: req.body.pType,
            isResidential: req.body.isResidential,
            pDescription: req.body.pDescription,
            pImage: req.body.pImage
        });
        newProperty.save(function(err) {
            if (err) {
                return res.json({
                    success:false,
                    message: 'An error has occured while saving.'
                });
            }
            res.json({
                success:true,
                message: 'Successfully created new property.'
            });
        })
    }
});

// Update existing properties.
router.put('/updateProperty', passport.authenticate('jwt', {
    session: false,
  }), function(req, res)
{
    if (!req.user.isEmployee) {
        res.json(
            {
                success: false,
                message: 'This space in unauthorised.'
            }
        );
    }
    else {
        Property.findByIdAndUpdate(req.body._id,
            {
                pLocation: req.body.pLocation,
                pPrice: req.body.pPrice,
                pType: req.body.pType,
                isResidential: req.body.isResidential,
                pDescription: req.body.pDescription,
                pImage: req.body.pImage
            }, function(err, property){
                if (err) throw err;
        
                if (!property) {
                    res.send({
                      success: false,
                      message: 'Update failed. Property not found.'
                    });
                  } else {
                    res.send({
                        success: true,
                        message: 'Update successfull.'
                      });
                  }
            })
    }
});

//Delete existing properties
router.delete('/deleteProperty/:_id', passport.authenticate('jwt', {
    session: false,
  }), function(req, res)
{
    if (!req.user.isEmployee) {
        res.json(
            {
                success: false,
                message: 'This space in unauthorised.'
            }
        );
    }
    else {
        Property.findByIdAndRemove(req.params._id,
            {
            }, function(err, property){
                if (err) throw err;
        
                if (!property) {
                    res.send({
                      success: false,
                      prop: req.params._id,
                      message: 'Delete failed. Property not found.'+req.params._id
                    });
                  } else {
                    res.send({
                        success: true,
                        message: 'Delete successfull.'
                      });
                  }
            })
    }
});

// Get the latest three properties. These are to be viewed in the homepage.
router.get('/getLastProperties', function(req, res)
{
    console.log("getLast");
Property.
  find({}).
  limit(3).
  sort({_id: -1 }).
  exec(function(err, properties)
  {
      res.json(properties);
  });
});


// Get properties. JWT is used to protect the properties window to only authenticated users.
router.get('/:_id', passport.authenticate('jwt', {
    session: false
  }), function(req, res) {
    Property.findById(req.params._id, {}, function(err, properties){
        //res.send('This is working');
        res.json(properties);
    });
  });


//Paginate reports
router.get('/page/:_id', passport.authenticate('jwt', {
    session: false
  }), function(req, res) {
    Property.paginate({}, { page: req.params._id, limit: 5 }, function(err, properties){
        //res.send('This is working');
        res.json({data: properties});
    });
  });

  // Get properties. JWT is used to protect the properties window to only authenticated users.
router.get('/', passport.authenticate('jwt', {
    session: false
  }), function(req, res) {
    Property.find({}, function(err, properties){
        //res.send('This is working');
        res.json(properties);
    });
  });


module.exports = router;