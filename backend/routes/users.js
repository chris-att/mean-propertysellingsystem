var express = require('express');
//require('use-strict');
var router = express.Router();
var User = require('../models/user');

let passport = require('passport');
let jwt = require('jsonwebtoken');

let config = require('../config');

// Register new users
router.post('/register', function(req, res) {
  console.log(req.body.firstName);
    if (!req.body.email || !req.body.password) {
      console.log('Please enter email and password.');
      res.json({
        success: false,
        message: 'Please enter email and password.'
      });
    } else {
      let newUser = new User({
        //userId: req.body.userId,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phone: req.body.phone,
        password: req.body.password,
        sendNotifications: req.body.sendNotifications,
        sendCatalog: req.body.sendCatalog,
        isEmployee: req.body.isEmployee
      });
  
//       // Attempt to save the user
      newUser.save(function(err) {
        if (err) {
          console.log('That email address already exists.');
          return res.json({
            success: false,
            message: 'That email address already exists.'
          });
        }
        console.log('Successfully created new user.');
        res.json({
          success: true,
          message: 'Successfully created new user.'
        });
      });
    }
  });
  
  router.get('/', function(req, res) {
    User.find({}, function(err, users) {
      res.json(users);
    });
  });

// Authenticate the user and get a JSON Web Token to include in the header of future requests.
router.post('/auth', (req, res) => {
    User.findOne({
      email: req.body.email
    }, function(err, user) {
      if (err) throw err;
  
      if (!user) {
        res.send({
          success: false,
          message: 'Authentication failed. User not found.'
        });
      } else {
        // Check if password matches
        user.comparePassword(req.body.password, function(err, isMatch) {
        if(user.password==req.body.password) console.log('1'+isMatch);
          if (isMatch && !err) {
            // Create token if the password matched and no error was thrown
            var token = jwt.sign(user.toJSON(), config.auth.secret, {
              expiresIn: "2 days"
            });
            res.json({
              success: true,
              message: 'Authentication successfull. Welcome '+user.firstName,
              isEmployee: user.isEmployee,
              token
            });
          } else {
            res.send({
              success: false,
              message: 'Authentication failed. Passwords did not match.'
            });
          }
        });
      }
    });
  });

module.exports = router;