// Required packages.
require('use-strict');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors');

var passport = require('passport');

var config = require('./config');

var routes = require('./routes/index');
var users = require('./routes/users');
var properties = require('./routes/properties');

var app = express();

//Connecting to the database.
mongoose.connect(config.database.mLab, err=> {
    if(!err){
        console.log('connected to mongodb');
    }
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Initialise CORS
app.use(cors());

//Initialise passport.
app.use(passport.initialize());
require('./config/passport')(passport);

//Routes.
app.use('/', routes);
app.use('/users', users);
app.use('/properties', properties);

app.listen(3000);

module.exports = app;